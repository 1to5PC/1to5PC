# A GitHub profile readme file<sup>[^1]</sup>
```
$ git add .
$ git commit -m "updated README"
$ git push
```
- [x] Use linux
- [x] Learn git

 [^1]: A unfinished one at that
<!--
**1to5pc/1to5PC** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
